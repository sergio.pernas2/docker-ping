# docker-ping

heramienta de prueba de conectividad.


#### Repositorio

Clonar repositorio.

```
$ git clone https://gitlab.com/sergio.pernas2/docker-ping.git

$ cd docker-ping
```


### Dockerfile.simple

Este fichero contruye una imagen con el comando 'ping 8.8.8.8'.

Crear imagen.

```
$ docker build -f Dockerfile.simple -t docker-ping-simple .
```

Lanzar contenedor.

```
$ docker run -d --name container_name docker-ping-simple
```

Ver salida del comando ping.

```
$ docker logs container_name
```



### Dockerfile.advanced


Crear imagen.

```
$ docker build -f Dockerfile.advanced -t docker-ping-advanced .
```

Lanzar contenedor.

```
$ docker run -d -e DHOST=IP/HOSTNAME --name container_name docker-ping-advanced
```

Ver salida del comando ping.

```
$ docker logs container_name
```

Punto de montaje.

```
$ touch salida.txt

$ docker run -d \
-e DHOST=google.com \
--mount type=bind,source="$(pwd)"/salida.txt,target=/ping.output \
docker-ping-advanced
```




